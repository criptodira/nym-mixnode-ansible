# Nym Mix Node - Ansible Playbook

Playbook de Ansible para instalar y configurar un nodo de mezcla en la red
de Nym (MixNode).

Primero clona el repositorio:

```
git clone https://0xacab.org/criptodira/nym-mixnode-ansible.git
cd nym-mixnode-ansible
```

Luego instala las dependencias:

```bash
ansible-galaxy install -r requirements.yml
ansible-galaxy collection verify -r requirements.yml
```

Crea manualmente un usuarix con el nombre `ansible` en tu servidor, agrégale a
la lista de sudoers y pon una llave ssh en `.ssh/authorized_keys`.

```bash
useradd -m -s /bin/bash ansible
```
También puedes agregarlo usando CloudInit si tu servidor lo soporta:

```bash
#cloud-config

users:
    - name: ansible
      lock_passwd: true
      shell: /bin/bash
      ssh_authorized_keys:
        - ${ssh_key}
      sudo: ALL=(ALL) NOPASSWD:ALL
```

Luego agrega la IP de tu nodo al inventario almacenado en `inventory/hosts`, e.g:

```bash
[mixnode]
10.8.0.1 ansible_user=ansible
```

Cambios que se aplican con éste playbook:

*Rol `init.yml`*

* Hardening del servicio de SSH (`devsec.hardening`).
* Instalación de `git`, `curl` y `wget`.

*Rol `mixnode.yml`*

* Crea el directorio `/opt/nym`, descarga el binario de github y verifica con el hash publicado en la página de descargas de Nym.
* Activa el firewall y aplica las reglas necesarias para los servicios de SSH, Nym y el monitoreo remoto.
* Agrega una unidad llamada `nym-mixnode` como servicio de systemd.

Ejecuta el playbook que quieras aplicar:

```bash
# Instala las dependencias básicas y refuerza la configuración de SSH.
ansible-playbook init.yml

# Instala nym-mixnode como servicio de systemd y habilita sus reglas de firewall.
ansible-playbook mixnode.yml
```

## Referencias
* https://nymtech.net/docs/
* https://nymtech.net/docs/nodes/mix-node-setup.html
* https://nymtech.net/docs/tools/nym-cli.html#bond-a-mix-node
* https://github.com/Noisk8/montando_nodo_nym/blob/main/README.md
